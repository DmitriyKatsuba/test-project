/**
 * This class is just a facade for your implementation, the tests below are using the `World` class only.
 * Feel free to add the data and behavior, but don't change the public interface.
 */


export class World {
  constructor() {
    this.powerPlants = [];
    this.households = [];    
  }

  getRootHousehold(id) {
    let household = this.households.find(item => item.id === id);
    if (household.root !== id) {
      household = this.getRootHousehold(household.root);
    }
    household.root = household.id;
    return household;
  }

  createPowerPlant() {
    const powerplant = new PowerPlant();
    this.powerPlants.push(powerplant)
    return powerplant;
  }

  createHousehold() {
    const houseHold = new HouseHold();
    this.households.push(houseHold)
    return houseHold;
  }

  connectHouseholdToPowerPlant(household, powerPlant) {
    const rootHousehold = this.getRootHousehold(household.id);
    rootHousehold.connectToPowerPlant(powerPlant);
    powerPlant.connectHouseHold(household);
  }

  connectHouseholdToHousehold(household1, household2) {
    const rootHousehold1 = this.getRootHousehold(household1.id);
    const rootHousehold2 = this.getRootHousehold(household2.id);
    if (rootHousehold1.id !== rootHousehold2.id){
      rootHousehold1.root = rootHousehold2.id;
      rootHousehold2.activePowerPlants += rootHousehold1.activePowerPlants
    }
  }

  disconnectHouseholdFromPowerPlant(household, powerPlant) {
    const rootHousehold = this.getRootHousehold(household.id);
    powerPlant.disconnectHouseHold(rootHousehold)
  }

  killPowerPlant(powerPlant) {
    const choozenPowerplant = this.powerPlants.find(
      (item) => item === powerPlant
    );
    if (choozenPowerplant.electricity) {
      choozenPowerplant.connectHouseHolds.forEach((household) => {
        const rootHousehold = this.getRootHousehold(household.id);
        rootHousehold.activePowerPlants--;
      });
      choozenPowerplant.electricity = false;
    }
  }

  repairPowerPlant(powerPlant) {
    const choozenPowerplant = this.powerPlants.find(
      (item) => item === powerPlant
    );
    if (!choozenPowerplant.electricity) {
      choozenPowerplant.connectHouseHolds.forEach((household) => {
        const rootHousehold = this.getRootHousehold(household.id);
        rootHousehold.activePowerPlants++;
      });
      choozenPowerplant.electricity = true;
    }
  }

  householdHasEletricity(household) {
    const rootHousehold = this.getRootHousehold(household.id);
    return rootHousehold.activePowerPlants > 0;
  }
}

export class PowerPlant {
  constructor() {
    this.electricity = true;
    this.connectHouseHolds = [];
  }

  connectHouseHold = (houseHold) => {
    this.connectHouseHolds.push(houseHold)
  }

  disconnectHouseHold = (houseHold) => {
    this.connectHouseHolds = this.connectHouseHolds.filter(item => item !== houseHold)
    houseHold.activePowerPlants--;
  }
}

export class HouseHold {
  constructor() {
    this.id = createID();
    this.root = this.id;
    this.activePowerPlants = 0;
  }
  connectToPowerPlant = (powerPlant) => {
    if (powerPlant.electricity) {
      this.activePowerPlants++;
    }
  };
}

const createID = () => {
  let date = new Date().getTime();
  let uuid = "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, (c) => {
    let r = (date + Math.random() * 16) % 16 | 0;
    date = Math.floor(date / 16);
    return (c == "x" ? r : (r & 0x3) | 0x8).toString(16);
  });
  return uuid;
}